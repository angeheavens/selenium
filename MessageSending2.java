package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class MessageSending2 {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://messagenet-services.webuat.sd-svc.net/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testMessageSending2() throws Exception {
    driver.get(baseUrl + "/Login/default.aspx");
    driver.findElement(By.id("ContentPlaceHolder1_txtUsername")).clear();
    driver.findElement(By.id("ContentPlaceHolder1_txtUsername")).sendKeys("admin");
    driver.findElement(By.id("ContentPlaceHolder1_txtPassword")).clear();
    driver.findElement(By.id("ContentPlaceHolder1_txtPassword")).sendKeys("Password1!");
    driver.findElement(By.id("ContentPlaceHolder1_btnLogin")).click();
    driver.findElement(By.linkText("Send Messages")).click();
    driver.findElement(By.cssSelector("span.ddArrow.arrowoff")).click();
    driver.findElement(By.xpath("//div[@id='DatabaseId_child']/ul/li[3]")).click();
    driver.findElement(By.id("Message")).clear();
    driver.findElement(By.id("Message")).sendKeys("testing send mesage");
    driver.findElement(By.id("StartDate")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-e")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-e")).click();
    driver.findElement(By.linkText("21")).click();
    driver.findElement(By.xpath("//div[@id='ui-datepicker-div']/div[2]/dl/dd[2]/div/a")).click();
    driver.findElement(By.xpath("//div[@id='ui-datepicker-div']/div[2]/dl/dd[3]/div/a")).click();
    driver.findElement(By.xpath("//div[@id='ui-datepicker-div']/div[2]/dl/dd[2]/div/a")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
    driver.findElement(By.id("add_recipients")).click();
    driver.findElement(By.id("SelectedContacts_5243")).click();
    driver.findElement(By.id("SelectedContacts_2921")).click();
    driver.findElement(By.id("SelectedContacts_4139")).click();
    driver.findElement(By.id("SelectedContacts_8127")).click();
    driver.findElement(By.id("SelectedContacts_3901")).click();
    driver.findElement(By.id("add_selection")).click();
    driver.findElement(By.cssSelector("input.button.pink")).click();
    driver.findElement(By.linkText("OK")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
