package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Contacts {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://messagenet-services.webuat.sd-svc.net/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testContacts() throws Exception {
    driver.get(baseUrl + "/Login/default.aspx");
    driver.findElement(By.id("ContentPlaceHolder1_txtPassword")).clear();
    driver.findElement(By.id("ContentPlaceHolder1_txtPassword")).sendKeys("Password1!");
    driver.findElement(By.id("ContentPlaceHolder1_txtUsername")).clear();
    driver.findElement(By.id("ContentPlaceHolder1_txtUsername")).sendKeys("admin");
    driver.findElement(By.id("ContentPlaceHolder1_btnLogin")).click();
    driver.findElement(By.linkText("Contacts")).click();
    driver.findElement(By.linkText("Create Contact")).click();
    driver.findElement(By.id("Name")).clear();
    driver.findElement(By.id("Name")).sendKeys("Jenomy");
    driver.findElement(By.id("JobTitle")).clear();
    driver.findElement(By.id("JobTitle")).sendKeys("QA test Analyst");
    driver.findElement(By.id("Phone")).clear();
    driver.findElement(By.id("Phone")).sendKeys("61412547896");
    driver.findElement(By.id("Email")).clear();
    driver.findElement(By.id("Email")).sendKeys("jenomy@test.net");
    driver.findElement(By.id("DateOfBirth")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-w")).click();
    driver.findElement(By.id("Gender_title")).click();
    driver.findElement(By.xpath("//div[@id='Gender_child']/ul/li[2]")).click();
    driver.findElement(By.id("Gender_title")).click();
    driver.findElement(By.xpath("//div[@id='Gender_child']/ul/li[3]/span")).click();
    driver.findElement(By.id("Groups1")).click();
    driver.findElement(By.cssSelector("#Country_msdd > div.ddTitle.borderRadiusTp > span.ddArrow.arrowoff")).click();
    driver.findElement(By.xpath("//div[@id='Country_child']/ul/li[15]/span")).click();
    driver.findElement(By.id("Country_titleText")).clear();
    driver.findElement(By.id("Country_titleText")).sendKeys("aus");
    driver.findElement(By.cssSelector("input.button.pink")).click();
    driver.findElement(By.linkText("Back To List")).click();
    driver.findElement(By.xpath("//div[@id='DataTable']/div/table/tbody/tr[5]/td[7]/a[2]")).click();
    driver.findElement(By.linkText("Cancel")).click();
    driver.findElement(By.id("cboxClose")).click();
    driver.findElement(By.xpath("//div[@id='DataTable']/div/table/tbody/tr[5]/td[7]/a[2]")).click();
    driver.findElement(By.linkText("Confirm")).click();
    driver.findElement(By.linkText("Close")).click();
    driver.findElement(By.id("cboxClose")).click();
    driver.findElement(By.id("export")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
