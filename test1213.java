package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Test1213 {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://messagenet-services.webuat.sd-svc.net/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void test1213() throws Exception {
    driver.get(baseUrl + "/MyAccount");
    driver.findElement(By.linkText("Send Messages")).click();
    driver.findElement(By.id("DatabaseId_title")).click();
    driver.findElement(By.xpath("//div[@id='DatabaseId_child']/ul/li[2]/span")).click();
    driver.findElement(By.id("DatabaseId_title")).click();
    driver.findElement(By.xpath("//div[@id='DatabaseId_child']/ul/li[3]/span")).click();
    driver.findElement(By.id("Message")).clear();
    driver.findElement(By.id("Message")).sendKeys("Test");
    driver.findElement(By.cssSelector("#PreTypedMessages_title > span.ddlabel")).click();
    driver.findElement(By.xpath("//div[@id='PreTypedMessages_child']/ul/li[2]/span")).click();
    driver.findElement(By.id("StartDate")).click();
    driver.findElement(By.cssSelector("a.ui-state-default.ui-state-hover")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
    driver.findElement(By.id("add_recipients")).click();
    driver.findElement(By.id("SelectedContacts_9513")).click();
    driver.findElement(By.id("SelectedContacts_5335")).click();
    driver.findElement(By.id("add_selection")).click();
    driver.findElement(By.cssSelector("input.button.pink")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
