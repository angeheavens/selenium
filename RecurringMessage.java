package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class RecurringMessage {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://messagenet-services.webuat.sd-svc.net/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testRecurringMessage() throws Exception {
    driver.get(baseUrl + "/Login/default.aspx");
    driver.findElement(By.id("ContentPlaceHolder1_txtUsername")).clear();
    driver.findElement(By.id("ContentPlaceHolder1_txtUsername")).sendKeys("admin");
    driver.findElement(By.id("ContentPlaceHolder1_txtPassword")).clear();
    driver.findElement(By.id("ContentPlaceHolder1_txtPassword")).sendKeys("Password1!");
    driver.findElement(By.id("ContentPlaceHolder1_btnLogin")).click();
    driver.findElement(By.linkText("Recurring Messages")).click();
    driver.findElement(By.linkText("Create Recurring Message")).click();
    driver.findElement(By.xpath("//div[@id='pretyed_message_child']/ul/li[2]/span")).click();
    driver.findElement(By.xpath("//div[@id='pretyed_message_child']/ul/li[2]/span")).click();
    driver.findElement(By.id("Message")).clear();
    driver.findElement(By.id("Message")).sendKeys("Testing recurring message 25% discount on selected items");
    driver.findElement(By.cssSelector("#BookedType_msdd > div.ddTitle.borderRadiusTp > span.ddArrow.arrowoff")).click();
    driver.findElement(By.xpath("//div[@id='BookedType_child']/ul/li[3]/span")).click();
    driver.findElement(By.cssSelector("#DayNumber_msdd > div.ddTitle.borderRadiusTp > span.ddArrow.arrowoff")).click();
    driver.findElement(By.xpath("//div[@id='DayNumber_child']/ul/li[4]")).click();
    driver.findElement(By.id("Time")).click();
    driver.findElement(By.id("Time")).clear();
    driver.findElement(By.id("Time")).sendKeys("19:20");
    driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
    driver.findElement(By.id("add_recipients")).click();
    driver.findElement(By.id("SelectedContacts_10747")).click();
    driver.findElement(By.id("SelectedContacts_11001")).click();
    driver.findElement(By.id("SelectedContacts_11028")).click();
    driver.findElement(By.id("SelectedContacts_10806")).click();
    driver.findElement(By.id("SelectedContacts_10806")).click();
    driver.findElement(By.id("add_selection")).click();
    driver.findElement(By.cssSelector("input.button.pink")).click();
    driver.findElement(By.id("SelectedContacts_8")).click();
    driver.findElement(By.id("delete")).click();
    driver.findElement(By.id("confirm")).click();
    driver.findElement(By.linkText("Cancel")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
